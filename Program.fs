﻿let rec validateBrackets inputChars bracketStack index =
    match inputChars with
    | [] ->
        if inputChars.Length > 0 then
            index
        else
            -1
    | x::xs ->
        if x = '(' || x = '{' || x = '[' || x = '<' then
            validateBrackets xs (x::bracketStack) (index + 1)
        elif x = ')' || x = '}' || x = ']' || x = '>' then
            if bracketStack.Length = 0 then
                index
            else
                if x =  ')' && (List.head bracketStack) <> '(' then
                    index
                elif x = '}' && (List.head bracketStack) <> '{' then
                    index
                elif x = ']' && (List.head bracketStack) <> '[' then
                    index
                elif x = '>' && (List.head bracketStack) <> '<' then
                    index
                else
                    validateBrackets xs (List.tail bracketStack) (index + 1)
        else
            index

let input = System.IO.File.ReadAllText("./input.txt")

let index =validateBrackets (Seq.toList input) [] 0

if index <> -1 then
    printfn "%A" index
else
    printfn "The input is valid"