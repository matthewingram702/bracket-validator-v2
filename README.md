# bracket-validator

To run locally:

1. Clone the repository
2. From the root directory run `dotnet run`
3. The output should be printed in the terminal

To run using Docker:

1. Clone the repository
2. From the root directory run `docker build -t bracket-validator .`
3. Run `docker run bracket-validator`
4. The output should be printed in the terminal

The provided `input.txt` file is loaded as the default input value.
